package greenmile.com;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import greenmile.br.model.Organizer;
import greenmile.br.service.impl.OrganizerServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest (webEnvironment = WebEnvironment.RANDOM_PORT)
public class GreenmileApplicationTests {
	
	@TestConfiguration
    static class TestContextConfiguration {
  
        @Bean
        public OrganizerServiceImpl organizadorService() {
            return new OrganizerServiceImpl();
        }
    }
	
	@Autowired
	private OrganizerServiceImpl service;
	
	@Test
	public void organizer() {
		Organizer organizer = new Organizer();
		organizer.setEmail("alan@gmail.com");
		organizer.setName("alan");
		organizer.setPassword("123");
		service.save(organizer);
		assertEquals(service.getByEmail("alan@gmail.com").getName(), "alan");
	}

}
