package greenmile.com;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import greenmile.br.model.Organizer;
import greenmile.br.model.Participant;
import greenmile.br.service.impl.OrganizerServiceImpl;
import greenmile.br.service.impl.ParticipantServiceImpl;

public class Teste {
	@Autowired
	private OrganizerServiceImpl service;
	
	private MockMvc mockMvc;

	@Test
	public void teste() {
		fail("Not yet implemented");
	}
	
	@Test
	public void organizer() {
		Organizer organizer = new Organizer();
		organizer.setEmail("alan@gmail.com");
		organizer.setName("alan");
		organizer.setPassword("123");
		service.save(organizer);
		assertEquals(service.getByEmail("alan@gmail.com").getName(), "alan");
	}

}
