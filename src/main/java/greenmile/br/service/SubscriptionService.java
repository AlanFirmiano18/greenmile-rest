package greenmile.br.service;

import java.util.List;

import greenmile.br.model.Subscription;

public interface SubscriptionService {
	
	public String save(Subscription subscription);
	
	public String delete(Integer id);
	
	public String update(Subscription subscription);
	
	public Subscription get(int id);
	
	public List<Subscription> get();
}
