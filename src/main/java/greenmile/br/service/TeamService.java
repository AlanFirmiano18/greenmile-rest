package greenmile.br.service;

import java.util.List;

import greenmile.br.model.Team;

public interface TeamService {
	
	public String save(Team team, Integer id);
	
	public String delete(Integer id);
	
	public String update(Team team);
	
	public Team get(int id);
	
	public List<Team> get();
}
