package greenmile.br.service;

import java.util.List;

import greenmile.br.model.User;

public interface UserService {
	
	public String save(User user);
	
	public String delete(int id);
	
	public String update(User user);
	
	public User get(int id);
	
	public User getByEmail(String email);
	
	public List<User> get();
}
