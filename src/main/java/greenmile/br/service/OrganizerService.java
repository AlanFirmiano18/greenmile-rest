package greenmile.br.service;

import java.util.List;

import greenmile.br.model.Organizer;

public interface OrganizerService {

	public String save(Organizer organizer);
	
	public String delete(Integer id);
	
	public String update(Organizer organizer);
	
	public Organizer get(int id);
	
	public Organizer getByEmail(String email);
	
	public List<Organizer> get();
	
}
