package greenmile.br.service;

import java.util.List;

import greenmile.br.model.Event;

public interface EventService {
	
	public String save(Event event);
	
	public String delete(Integer id);
	
	public String update(Event event);
	
	public String finalize(Integer idEvent);
	
	public Event get(int id);
	
	public List<Event> getByStatus(boolean status);
	
	public List<Event> get();
}
