package greenmile.br.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import greenmile.br.model.Participant;

public interface ParticipantService {
	
	public String save(Participant participant, Integer event);
	
	public String uploadFile(int id, MultipartFile file);
	
	public String delete(Integer id);
	
	public String update(Participant participant);
	
	public Participant get(int id);
	
	public Participant getByEmail(String email);
	
	public List<Participant> get();
}
