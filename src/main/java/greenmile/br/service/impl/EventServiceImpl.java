package greenmile.br.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import greenmile.br.model.Event;
import greenmile.br.repository.EventRepository;
import greenmile.br.service.EventService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class EventServiceImpl implements EventService{
	
	@Autowired
    EventRepository eventRepository;

    public String save(Event event){
    	if(null!=eventRepository.findByName(event.getName()))
    		return ConfigurationConstants.MSG_ERR_EVENTO_CADASTRADO;
        event.setStatus(true);
    	eventRepository.save(event);
        return ConfigurationConstants.MSG_EVENTO_SAVE;
    }

    public String delete(Integer id){
        eventRepository.deleteById(id);
        return ConfigurationConstants.MSG_EVENTO_REMOVE;
    }

    public String update(Event event){
        eventRepository.save(event);
        return ConfigurationConstants.MSG_EVENTO_UPDATE;
    }

    public String finalize(Integer idEvent){
        Event event = eventRepository.findById(idEvent).get();
        if(event==null)
        	return ConfigurationConstants.MSG_ERR_EVENTO_NAO_ENCONTRADO;
        if(event.isStatus()) 
        	event.setStatus(false); 
        else
        	return ConfigurationConstants.MSG_ERR_EVENTO_JA_ENCERRADO;
        
        eventRepository.save(event);
        return ConfigurationConstants.MSG_EVENTO_ENCERRAR;
    }
    
    public Event get(int id){
        return this.eventRepository.findById(id).get();
    }

    public List<Event> getByStatus(boolean status){
        return this.eventRepository.findByStatus(status);
    }
    
    public List<Event> get(){
        return this.eventRepository.findAll();
    }
}