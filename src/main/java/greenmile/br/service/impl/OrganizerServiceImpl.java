package greenmile.br.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import greenmile.br.model.Organizer;
import greenmile.br.model.Role;
import greenmile.br.repository.OrganizerRepository;
import greenmile.br.repository.RoleRepository;
import greenmile.br.service.OrganizerService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class OrganizerServiceImpl implements OrganizerService{
	
	@Autowired
    OrganizerRepository organizerRepository;

	@Autowired
	RoleRepository roleRepository;
	
    public String save(Organizer organizer){
    	Organizer aux = organizerRepository.findByEmail(organizer.getEmail());
    	List<Role> roles = roleRepository.findAll();
    	Role role = new Role(ConfigurationConstants.ORGANIZADOR);
    	if(!roles.isEmpty())
    		for(Role roleAux : roles)
    			if(roleAux.getDescription().equals(ConfigurationConstants.ORGANIZADOR)) 
    				role = roleAux;
    	else 
    		role = roleRepository.save(role);
    	
    	if(null != aux) 
    		return ConfigurationConstants.MSG_ERR_ORGANIZADOR_CADASTRADO;
    	
    	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    	organizer.setPassword(encoder.encode(organizer.getPassword()));
		
    	organizer.add(role);
		
    	organizerRepository.save(organizer);
    	return ConfigurationConstants.MSG_ORGANIZADOR_SAVE;
    }

    public String delete(Integer id){
        organizerRepository.deleteById(id);
        return ConfigurationConstants.MSG_ORGANIZADOR_REMOVE;
    }

    public String update(Organizer organizer){
        organizerRepository.save(organizer);
        return ConfigurationConstants.MSG_ORGANIZADOR_UPDATE;
    }

    public Organizer get(int id){
        return this.organizerRepository.findById(id).get();
    }
    
    public Organizer getByEmail(String email){
        return this.organizerRepository.findByEmail(email);
    }

    public List<Organizer> get(){
        return this.organizerRepository.findAll();
    }
}