package greenmile.br.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import greenmile.br.model.Team;
import greenmile.br.model.Event;
import greenmile.br.model.Subscription;
import greenmile.br.model.Participant;
import greenmile.br.repository.TeamRepository;
import greenmile.br.repository.EventRepository;
import greenmile.br.repository.SubscriptionRepository;
import greenmile.br.service.TeamService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class TeamServiceImpl implements TeamService{
	
	@Autowired
    TeamRepository teamRepository;
	
	@Autowired
	SubscriptionRepository subscriptionRepository;
	
	@Autowired
	EventRepository eventRepository;
	
    public String save(Team team, Integer id){
    	
    	if(!eventRepository.findById(id).isPresent())
    		return ConfigurationConstants.MSG_ERR_EVENTO_NAO_ENCONTRADO;
    	if(subscriptionRepository.findByTeamNameAndEventId(team.getName(), id) != null) 
    		return ConfigurationConstants.MSG_ERR_EQUIPE_CADASTRADA;
        Event event = eventRepository.findById(id).get();
        if(team.getParticipants()==null || team.getParticipants().isEmpty()) 
    		return ConfigurationConstants.MSG_ERR_EQUIPE_NAO_POSSUI_PARTICIPANTES;
    	if(team.getParticipants().size() > event.getNumberOfParticipants())
    		return ConfigurationConstants.MSG_ERR_EQUIPE_EXCEDENDO_LIMITE;
    	
    	for(Participant participant: team.getParticipants())
    		if(null==participant.getPassword() || participant.getPassword().equals(ConfigurationConstants.SENHA_PADRAO)) {
    			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    	    	participant.setPassword(encoder.encode(ConfigurationConstants.SENHA_PADRAO));
    		}
        subscriptionRepository.save(new Subscription(new Date(), teamRepository.save(team), eventRepository.findById(id).get()));
        return ConfigurationConstants.MSG_EQUIPE_SAVE;
    }

    public String delete(Integer id){
        teamRepository.deleteById(id);
        return ConfigurationConstants.MSG_EQUIPE_REMOVE;
    }

    public String update(Team team){
        teamRepository.save(team);
        return ConfigurationConstants.MSG_EQUIPE_UPDATE;
    }
    
    public Team get(int id){
        return this.teamRepository.findById(id).get();
    }

    public List<Team> get(){
        return this.teamRepository.findAll();
    }
}