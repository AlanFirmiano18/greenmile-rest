package greenmile.br.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import greenmile.br.model.Role;
import greenmile.br.model.Participant;
import greenmile.br.model.User;
import greenmile.br.repository.SubscriptionRepository;
import greenmile.br.repository.OrganizerRepository;
import greenmile.br.repository.RoleRepository;
import greenmile.br.repository.ParticipantRepository;
import greenmile.br.repository.UserRepository;
import greenmile.br.service.ParticipantService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class ParticipantServiceImpl implements ParticipantService{
	
	@Autowired
    ParticipantRepository participantRepository;

	@Autowired
	OrganizerRepository organizerRepository;
	
	@Autowired
	SubscriptionRepository subscriptionRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
    public String save(Participant participant, Integer event){
    	User user = userRepository.findByEmail(participant.getEmail());
    	List<Role> roles = roleRepository.findAll();
    	Role role = new Role();
    	if(!roles.isEmpty())
    		for(Role roleAux : roles)
    			if(roleAux.getDescription().equals(ConfigurationConstants.PARTICIPANTE)) 
    				role = roleAux;
    	else 
    		role = roleRepository.save(new Role(ConfigurationConstants.PARTICIPANTE));
    	
    	if(null != user) {
    		user.add(role);
    		userRepository.save(user);
    		return ConfigurationConstants.MSG_ADD_PAPEL_PARTICIPANTE;
    	}
    	
    	if(null != subscriptionRepository.findByTeamParticipantsEmailAndEventId(participant.getEmail(), event))
    		return ConfigurationConstants.MSG_ERR_PARTICIPANTE_CADASTRADO;
    	
    	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    	participant.setPassword(encoder.encode(participant.getPassword()));
    	
    	participant.add(role);
    	participantRepository.save(participant);
    	return ConfigurationConstants.MSG_PARTICIPANTE_SAVE;
    }

    public String uploadFile(int id, MultipartFile file) {
    	File convertFile = new File("");
		if(null==file)
			return ConfigurationConstants.MSG_ERR_ARQUIVO_NAO_SUPORTADO;
		
		convertFile = new File(this.getClass().getClassLoader().getResource("public/foto/").getPath()+file.getOriginalFilename());
		try {
			convertFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(convertFile);
			
			fout.write(file.getBytes());
			fout.close();
			
			Participant participant = participantRepository.findById(id).get();
			participant.setPathFoto("http://localhost:8080/foto/"+file.getOriginalFilename());
			participantRepository.save(participant);
			
			return ConfigurationConstants.MSG_ADD_FOTO;
			
		} catch (IOException e) {
			return e.getMessage();
		}
    }
    
    public String delete(Integer id){
        participantRepository.deleteById(id);
        return ConfigurationConstants.MSG_PARTICIPANTE_REMOVE;
    }

    public String update(Participant participant){
        participantRepository.save(participant);
        return ConfigurationConstants.MSG_PARTICIPANTE_UPDATE;
    }

    public Participant get(int id){
        return this.participantRepository.findById(id).get();
    }

    public Participant getByEmail(String email){
        return this.participantRepository.findByEmail(email);
    }

    public List<Participant> get(){
        return this.participantRepository.findAll();
    }   
}