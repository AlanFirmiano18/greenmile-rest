package greenmile.br.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import greenmile.br.model.Subscription;
import greenmile.br.repository.EventRepository;
import greenmile.br.repository.SubscriptionRepository;
import greenmile.br.service.SubscriptionService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class SubscriptionServiceImpl implements SubscriptionService{
	
	@Autowired
    SubscriptionRepository subscriptionRepository;
	
	@Autowired
	EventRepository eventRepository;
	
    public String save(Subscription subscription){
        if(!eventRepository.findById(subscription.getEvent().getId()).get().isStatus()) {
        	return ConfigurationConstants.MSG_ERR_EVENTO_ENCERRADO;
        }
    	subscriptionRepository.save(subscription);
        return ConfigurationConstants.MSG_INSCRICAO_SAVE;
    }

    public String delete(Integer id){
        subscriptionRepository.deleteById(id);
        return ConfigurationConstants.MSG_INSCRICAO_REMOVE;
    }

    public String update(Subscription subscription){
    	if(!eventRepository.findById(subscription.getEvent().getId()).get().isStatus()) {
        	return ConfigurationConstants.MSG_ERR_EVENTO_ENCERRADO;
        }
        subscriptionRepository.save(subscription);
        return ConfigurationConstants.MSG_INSCRICAO_UPDATE;
    }

    public Subscription get(int id){
        return this.subscriptionRepository.findById(id).get();
    }

    public List<Subscription> get(){
        return this.subscriptionRepository.findAll();
    }
}
