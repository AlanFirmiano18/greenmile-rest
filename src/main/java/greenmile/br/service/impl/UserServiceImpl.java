package greenmile.br.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import greenmile.br.model.User;
import greenmile.br.repository.UserRepository;
import greenmile.br.service.UserService;
import greenmile.br.util.ConfigurationConstants;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;
	
	public String save(User user) {
		userRepository.save(user);
		return ConfigurationConstants.MSG_USUARIO_SAVE;
	}

	public String delete(int id) {
		userRepository.deleteById(id);
		return ConfigurationConstants.MSG_USUARIO_REMOVE;
	}

	public String update(User user) {
		userRepository.save(user);
		return ConfigurationConstants.MSG_USUARIO_UPDATE;
	}

	public User get(int id) {
		return this.userRepository.findById(id).get();
	}

	public User getByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	public List<User> get() {
		return this.userRepository.findAll();
	}
}
