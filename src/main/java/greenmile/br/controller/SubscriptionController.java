package greenmile.br.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import greenmile.br.model.Subscription;
import greenmile.br.service.impl.SubscriptionServiceImpl;

@RestController
@RequestMapping("/subscriptions")
@CrossOrigin(origins = "*")
public class SubscriptionController {
	
	@Autowired
    private SubscriptionServiceImpl subscriptionService;

    @PostMapping
    public ResponseEntity<String> save(@RequestBody Subscription subscription){
        return ResponseEntity.ok(subscriptionService.save(subscription));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        return ResponseEntity.ok(subscriptionService.delete(id)); 
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody Subscription subscription){
        return ResponseEntity.ok(subscriptionService.update(subscription));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subscription> get(@PathVariable Integer id){
        return ResponseEntity.ok(subscriptionService.get(id));
    }
    
    @GetMapping
    public ResponseEntity<List<Subscription>> get(){
        return ResponseEntity.ok(subscriptionService.get());
    }
}