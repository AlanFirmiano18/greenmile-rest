package greenmile.br.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import greenmile.br.model.Organizer;
import greenmile.br.service.impl.OrganizerServiceImpl;

@RestController
@RequestMapping("/organizers")
@CrossOrigin(origins = "*")
public class OrganizerController {
	
	@Autowired
    private OrganizerServiceImpl organizerService;

    @PostMapping
    public ResponseEntity<String> save(@RequestBody Organizer organizer){
        return ResponseEntity.ok(organizerService.save(organizer));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        return ResponseEntity.ok(organizerService.delete(id));
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody Organizer organizer){
        return ResponseEntity.ok(organizerService.update(organizer));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Organizer> get(@PathVariable Integer id){
        return ResponseEntity.ok(organizerService.get(id));
    }
    
    @GetMapping
    public ResponseEntity<List<Organizer>> get(){
        return ResponseEntity.ok(organizerService.get());
    }
}
