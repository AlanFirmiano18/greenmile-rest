package greenmile.br.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import greenmile.br.model.Event;
import greenmile.br.service.impl.EventServiceImpl;

@RestController
@RequestMapping("/events")
@CrossOrigin(origins = "*")
public class EventController {
	
	@Autowired
    private EventServiceImpl eventService;

    @PostMapping
    public ResponseEntity<String> save(@RequestBody Event event){
        return ResponseEntity.ok(eventService.save(event));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        return ResponseEntity.ok(eventService.delete(id));
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody Event event){
        return ResponseEntity.ok(eventService.update(event));
    }
    
    @PutMapping("/finalize/{id}")
    public ResponseEntity<String> finalize(@PathVariable Integer id){
        return ResponseEntity.ok(eventService.finalize(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Event> get(@PathVariable Integer id){
        return ResponseEntity.ok(eventService.get(id));
    }

    @GetMapping("/available")
    public ResponseEntity<List<Event>> getAvailable(){
        return ResponseEntity.ok(eventService.getByStatus(true));
    }
    
    @GetMapping
    public ResponseEntity<List<Event>> get(){
        return ResponseEntity.ok(eventService.get());
    }
}