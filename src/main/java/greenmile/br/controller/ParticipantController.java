package greenmile.br.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import greenmile.br.model.Participant;
import greenmile.br.service.impl.ParticipantServiceImpl;

@RestController
@RequestMapping("/participants")
@CrossOrigin(origins = "*")
public class ParticipantController {
	
	@Autowired
	private ParticipantServiceImpl participantService;
	
	@PostMapping("/{id}")
	public ResponseEntity<String> save(@RequestBody Participant participant, @PathVariable Integer id){
		return ResponseEntity.ok(participantService.save(participant, id));
	}
	
	
	@PostMapping(value="/{id}/upload", headers = "content-type=multipart/form-data")
	public ResponseEntity<String> uploadFile(@PathVariable Integer id, @RequestParam("file") MultipartFile file) throws IOException{
		return ResponseEntity.ok(participantService.uploadFile(id, file));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable Integer id){
		return ResponseEntity.ok(participantService.delete(id));
	}

	@PutMapping
	public ResponseEntity<String> update(@RequestBody Participant participant){
		return ResponseEntity.ok(participantService.update(participant));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Participant> getById(@PathVariable Integer id){
		return ResponseEntity.ok(participantService.get(id));
	}

	@GetMapping("/email/{email}")
	public ResponseEntity<Participant> get(@PathVariable String email){
		return ResponseEntity.ok(participantService.getByEmail(email));
	}
	
	@GetMapping
	public ResponseEntity<List<Participant>> get(){
		return ResponseEntity.ok(participantService.get());
	}
}