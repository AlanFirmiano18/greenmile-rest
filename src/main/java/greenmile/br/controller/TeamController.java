package greenmile.br.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import greenmile.br.model.Team;
import greenmile.br.service.impl.TeamServiceImpl;

@RestController
@RequestMapping("/teams")
@CrossOrigin(origins = "*")
public class TeamController {
	
	@Autowired
    private TeamServiceImpl teamService;

    @PostMapping("/{idEvent}")
    public ResponseEntity<String> save(@RequestBody Team team, @PathVariable Integer idEvent){
        return ResponseEntity.ok(teamService.save(team, idEvent));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        return ResponseEntity.ok(teamService.delete(id));
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody Team team){
        return ResponseEntity.ok(teamService.update(team));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Team> get(@PathVariable Integer id){
        return ResponseEntity.ok(teamService.get(id));
    }

    @GetMapping
    public ResponseEntity<List<Team>> get(){
        return ResponseEntity.ok(teamService.get());
    }

}
