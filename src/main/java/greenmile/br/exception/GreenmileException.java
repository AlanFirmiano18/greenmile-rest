package greenmile.br.exception;

public class GreenmileException extends Exception{
	private static final long serialVersionUID = 1L;
	
	private String message;

	public GreenmileException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
