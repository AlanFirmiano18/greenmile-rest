package greenmile.br.util;

public class ConfigurationConstants {
	//SENHA DEFAULT PARA PARTICIPANTE
	public static final String SENHA_PADRAO="12345";
	//PAPEIS
	public static final String ORGANIZADOR="ORGANIZADOR";
	public static final String PARTICIPANTE="PARTICIPANTE";
	public static final String ADMINISTRADOR="ADMINISTRADOR";
	//MSG CRUD
	public static final String MSG_INSCRICAO_SAVE="Inscrição realizada com sucesso!";
	public static final String MSG_INSCRICAO_UPDATE="Inscrição atualizada com sucesso!";
	public static final String MSG_INSCRICAO_REMOVE="Inscrição cancelada!";
	
	public static final String MSG_USUARIO_SAVE="Usuario cadastrado com sucesso!";
	public static final String MSG_USUARIO_UPDATE="Usuario atualizado com sucesso!";
	public static final String MSG_USUARIO_REMOVE="Usuario removido com sucesso!";
	
	public static final String MSG_ORGANIZADOR_SAVE="Organizador cadastrado com sucesso!";
	public static final String MSG_ORGANIZADOR_UPDATE="Organizador atualizado com sucesso!";
	public static final String MSG_ORGANIZADOR_REMOVE="Organizador removido com sucesso!";
	
	public static final String MSG_EVENTO_SAVE="Evento cadastrado com sucesso!";
	public static final String MSG_EVENTO_UPDATE="Evento atualizado com sucesso!";
	public static final String MSG_EVENTO_ENCERRAR="Evento encerrado!";
	public static final String MSG_EVENTO_REMOVE="Evento removido com sucesso!";
	
	public static final String MSG_EQUIPE_SAVE="Equipe cadastrada com sucesso!";
	public static final String MSG_EQUIPE_UPDATE="Equipe atualizada com sucesso!";
	public static final String MSG_EQUIPE_REMOVE="Equipe removida com sucesso!";
	
	public static final String MSG_PARTICIPANTE_SAVE="Participante cadastrado com sucesso!";
	public static final String MSG_PARTICIPANTE_UPDATE="Participante atualizado com sucesso!";
	public static final String MSG_PARTICIPANTE_REMOVE="Participante removido com sucesso!";
	//ADD PAPEL
	public static final String MSG_ADD_PAPEL_PARTICIPANTE="Adicionado papel de Participante!";
	public static final String MSG_ADD_PAPEL_ORGANIZADOR="Adicionado papel de Organizador!";
	//ADD FOTO
	public static final String MSG_ADD_FOTO="Foto enviada com sucesso!";
	//MSG ERR
	public static final String MSG_ERR_INSCRICAO_CADASTRADA="Inscrição ja cadastrada!";
	public static final String MSG_ERR_ORGANIZADOR_CADASTRADO="Organizador ja cadastrado!";
	public static final String MSG_ERR_EVENTO_CADASTRADO="Evento ja cadastrado!";
	public static final String MSG_ERR_EVENTO_ENCERRADO="Evento indisponivel!";
	public static final String MSG_ERR_EVENTO_NAO_ENCONTRADO="Evento nao encontrado!";
	public static final String MSG_ERR_EVENTO_JA_ENCERRADO="Evento ja encerrado!";
	public static final String MSG_ERR_EQUIPE_CADASTRADA="Equipe ja cadastrada com esse nome!";
	public static final String MSG_ERR_EQUIPE_EXCEDENDO_LIMITE="Equipe possui muitos membros!";
	public static final String MSG_ERR_EQUIPE_NAO_POSSUI_PARTICIPANTES="Equipe não possui membros!";
	public static final String MSG_ERR_PARTICIPANTE_CADASTRADO="Participante ja cadastrado!";
	public static final String MSG_ERR_PARTICIPANTE_CADASTRADO_EVENTO="Participante ja cadastrado nesse evento!";
	public static final String MSG_ERR_USUARIO_CADASTRADO="Usuario ja cadastrado!";
	public static final String MSG_ERR_ARQUIVO_NAO_SUPORTADO="Arquivo não suportado!";
}
