package greenmile.br.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Subscription {
	@Id
	@GeneratedValue
	private Integer id;
	private Date date;
	@OneToOne
	private Team team;
	@OneToOne
	private Event event;
	
	public Subscription() {
		
	}
	
	public Subscription(Date date, Team team, Event event) {
		this.date = date;
		this.team = team;
		this.event = event;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	
}
