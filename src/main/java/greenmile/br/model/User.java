package greenmile.br.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements UserDetails{
	@Id
	@GeneratedValue
	private Integer id;
	@NotNull
	private String email;
	@NotNull
	private String password;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@Cascade(value = CascadeType.PERSIST)
	private List<Role> roles;
	
	public User() {
		roles = new ArrayList<>();
	}
	
	public User(String email,String password) {
		this.email = email;
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return email;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void add(Role role) {
		this.getRoles().add(role);
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
}