package greenmile.br.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer>{
	
	Optional<Team> findById(Integer id);
	
	Team findByName(String name);
	
}
