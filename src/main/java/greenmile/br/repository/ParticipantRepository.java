package greenmile.br.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Participant;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Integer>{
	
	Optional<Participant> findById(Integer id);
	
	Participant findByEmail(String email);
	
	
}
