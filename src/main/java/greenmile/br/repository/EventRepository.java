package greenmile.br.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Event;
import greenmile.br.model.Participant;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer>{
	
	Optional<Event> findById(Integer id);
	
	Event findByName(String name);
	
	List<Event> findByStatus(boolean status);
}
