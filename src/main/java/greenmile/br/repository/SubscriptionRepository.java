package greenmile.br.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Team;
import greenmile.br.model.Subscription;
import greenmile.br.model.Participant;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Integer>{
	
	Optional<Subscription> findById(Integer id);
	
	List<Subscription> findByEventId(Integer id);
	
	Subscription findByTeamNameAndEventId(String name, Integer event);
	
	Participant findByTeamParticipantsEmailAndEventId(String email, Integer event);
	
	@Query("FROM Subscription ORDER BY date")
	List<Subscription> findAll();
	
}
