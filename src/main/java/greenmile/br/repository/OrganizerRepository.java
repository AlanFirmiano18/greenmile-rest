package greenmile.br.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Organizer;

@Repository
public interface OrganizerRepository extends JpaRepository<Organizer, Integer>{
	
	Optional<Organizer> findById(Integer id);
	
	Organizer findByEmail(String email);

}
