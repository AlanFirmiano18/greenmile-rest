package greenmile.br.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	Optional<User> findById(Integer id);
	
	User findByEmail(String email);
	
}