package greenmile.br.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import greenmile.br.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer>{
	Role findByDescription(String description);
}
