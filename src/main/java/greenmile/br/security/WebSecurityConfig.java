package greenmile.br.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.filter.GenericFilterBean;

import greenmile.br.model.Role;
import greenmile.br.model.User;
import greenmile.br.repository.UserRepository;
import greenmile.br.util.ConfigurationConstants;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UsuarioDetailsService usuarioDetailsService;
    
    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
  
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
        //.antMatchers("/h2/**").permitAll()	
        .antMatchers(HttpMethod.POST, "/login", "/teams/**").permitAll()
        .antMatchers(HttpMethod.GET,"/foto/**","/events/avaliable").permitAll()
        
		.antMatchers(HttpMethod.POST, "/organizers/**").hasAuthority(ConfigurationConstants.ADMINISTRADOR)
		.antMatchers(HttpMethod.PUT, "/organizers/**").hasAnyAuthority(ConfigurationConstants.ADMINISTRADOR, ConfigurationConstants.ORGANIZADOR)
		.antMatchers(HttpMethod.DELETE, "/organizers/**").hasAnyAuthority(ConfigurationConstants.ADMINISTRADOR, ConfigurationConstants.ORGANIZADOR)
		.antMatchers(HttpMethod.POST, "/events/**").hasAnyAuthority(ConfigurationConstants.ORGANIZADOR)
		.antMatchers(HttpMethod.GET, "/events/**").hasAnyAuthority(ConfigurationConstants.ORGANIZADOR)
		.antMatchers(HttpMethod.DELETE, "/events/**").hasAnyAuthority(ConfigurationConstants.ORGANIZADOR)
		.antMatchers(HttpMethod.PUT, "/events/**").hasAnyAuthority(ConfigurationConstants.ORGANIZADOR)
        
		.anyRequest().hasAnyAuthority(ConfigurationConstants.ADMINISTRADOR, ConfigurationConstants.ORGANIZADOR, ConfigurationConstants.PARTICIPANTE)
        
        .and()
        .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(new JWTAuthenticationFilter(),
                UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String result = encoder.encode("admin123");
        
        auth.inMemoryAuthentication()
                .withUser("zara@ufc.com")
                .password(result)
                .authorities(ConfigurationConstants.ADMINISTRADOR);
        auth.userDetailsService(usuarioDetailsService).passwordEncoder(passwordEncoder());
    }

}