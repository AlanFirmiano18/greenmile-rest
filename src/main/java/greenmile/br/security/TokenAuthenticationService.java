package greenmile.br.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;

import greenmile.br.model.Role;
import greenmile.br.model.User;
import greenmile.br.repository.UserRepository;
import greenmile.br.util.ConfigurationConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@CrossOrigin(origins = "*", maxAge = 3600) 
class TokenAuthenticationService {
    static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "!@GREEN#API#HACK!@";
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";

    public static void addAuthentication(HttpServletResponse res, String username) throws IOException {
        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        res.getOutputStream().write((TOKEN_PREFIX + JWT).getBytes());
    }
    
    public static UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, 
    		HttpServletResponse response, UserRepository repository) throws IOException {
        
    	String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String user = null;
            try {
                user = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();
            }catch (Exception e) {
            	
            }
            
            if(null!=user) {
            	User userAux = new User();
            	Role role = new Role();
            	role.setDescription(ConfigurationConstants.ADMINISTRADOR);
            	System.err.println(user);
            	if(!user.equals("zara@ufc.com")) {
            		userAux = repository.findByEmail(user);
            	}else if(user.equals("zara@ufc.com")){
            		userAux.add(role);
            	}
            	return new UsernamePasswordAuthenticationToken(user, null, userAux.getAuthorities());
            
            }return null;
        }
        return null;
    }
}